<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
   <h2>Latihan Looping</h2>
   <?php
    echo "<h3> Soal No 1 </h3>";
    echo "<h4>Looping 1</h4>";
    for ($i=1; $i <=21; $i+=2) {
        echo $i . " - I Love PHP <br>";
    }
    echo "<h4>Looping 2</h4>";
    for ($a = 21; $a >=1; $a -=2) {
        echo $a . " - I Love PHP <br>";
    }

    echo "<h3> Soal No 2 <br> Looping Array Modulo </h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo " Array : <br>";
    print_r($number);
    echo "<br>";
    foreach($number as $number) {
    $rest[] = $number * 2;
    }
    print_r($rest);

    echo "<h3> Soal No 3 <br> Loop Associative Arra </h3>";
    $items = [
        [001, "Keyboard Logitek", 60000, "Keyboard yang Mantab untuk Kantoran","Logitek.jpeg"],
        [002, "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik","msi.jpeg"],
        [003, "Mouse Genius", 50000, "Mouse Genius biar lebih pinter","genius.jpeg"],
        [004, "Mouse jerry", 30000, "Mouse yang disukai kucing","jerry.jpeg"],
    ];
    foreach ($items as $key =>$val) {
        $items = array (
            'id' => $val[0],
            'name' => $val[1],
            'price' => $val[2],
            'description' => $val[2],
            'source' => $val[2]
        );
        print_r($items);
        echo "<br>";
    }

    echo "<h3> Soal No 4 Asterix </h3>";
    for ($j= 1;  $j <=5 ; $j++){
        for ($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
   ?>

</body>
</html>